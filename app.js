const express = require('express')
const path = require('path')

const sessionMiddleware = require('./libs/express-session.js')

const homeRoute = require('./routes/home.js')
const loginRoute = require('./routes/login.js')
const registrationRoute = require('./routes/registration.js')

const app = express()

app.set('views', path.resolve('views'))
app.set('view engine', 'pug')

app.use(express.static('public'))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(sessionMiddleware())

app.use('/', homeRoute)
app.use('/login', loginRoute)
app.use('/register', registrationRoute)

app.get('/logout', function(req, res, next) {
  req.session.destroy(err => {
    if (err) next(err)
    res.redirect('back')
  })
})

app.use((req, res, next) => {
  res.render('404')
})

app.use((err, req, res, next) => {
  console.error(err)

  res.render('500')
})

module.exports = app
