const config = require('./server.js')

const debug = process.env.DEBUG

module.exports = {
  dialect: 'mysql',
  host: config.get('database.host'),
  port: config.get('database.port'),
  database: config.get('database.name'),
  username: config.get('database.user'),
  password: config.get('database.password'),
  logging: debug ? console.log : false
}
