require('./env.js')

const convict = require('convict')

const config = convict({
  env: {
    doc: 'Environment',
    format: String,
    default: 'development',
    env: 'NODE_ENV'
  },
  port: {
    doc: 'Application Port',
    format: 'port',
    default: 3000,
    env: 'PORT'
  },
  database: {
    host: {
      doc: 'Database Host',
      format: 'ipaddress',
      default: '127.0.0.1',
      env: 'DB_HOST'
    },
    port: {
      doc: 'Database Port',
      format: 'port',
      default: 3306,
      env: 'DB_PORT'
    },
    name: {
      doc: 'Database Name',
      format: String,
      default: 'db',
      env: 'DB_NAME'
    },
    user: {
      doc: 'Database User',
      format: String,
      default: 'root',
      env: 'DB_USER'
    },
    password: {
      doc: 'Database Password',
      format: String,
      default: 'root',
      env: 'DB_PASSWORD'
    }
  },
  express: {
    killTimeout: {
      doc: 'Kill Timeout',
      format: 'nat',
      default: 2000,
      env: 'EXPRESS_KILL_TIMEOUT'
    }
  }
})

config.validate({ allowed: 'strict' })

module.exports = config
