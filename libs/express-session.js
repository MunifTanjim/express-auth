const session = require('express-session')

const SequelizeSessionStore = require('connect-session-sequelize')(
  session.Store
)

const { sequelize } = require('../database/index.js')

const sessionStore = new SequelizeSessionStore({
  db: sequelize,
  checkExpirationInterval: 15 * 60 * 1000,
  expiration: 24 * 60 * 60 * 1000
})

sessionStore.sync()

const options = {
  name: 'expressAuth.sid',
  resave: false,
  saveUninitialized: false,
  secret: 'expressAuth',
  store: sessionStore,
  unset: 'destroy'
}

const sessionMiddleware = () => session(options)

module.exports = sessionMiddleware
