const crypto = require('crypto')

const hash = password => {
  const hashedPassword = crypto
    .createHash('sha256')
    .update(password)
    .digest('hex')

  return hashedPassword
}

const verify = (passwordHash, password) => {
  const result = passwordHash === hash(password)

  return result
}

module.exports.hashPassword = hash
module.exports.verifyPassword = verify
