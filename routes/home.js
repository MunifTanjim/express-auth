const router = require('express').Router()

const { User } = require('../database/index.js')

router.get('/', (req, res, next) => {
  const data = {}

  if (req.session.user) {
    return User.findByPk(req.session.user.id).then(user => {
      data.user = user.toJSON()

      return res.render('index', data)
    })
  }

  res.render('index', data)
})

module.exports = router
