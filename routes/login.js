const router = require('express').Router()

const { body } = require('express-validator/check')
const { validationResult } = require('express-validator/check')
const { matchedData } = require('express-validator/filter')

const { verifyPassword } = require('../libs/password.js')

const { User } = require('../database/index.js')

router.get('/', function(req, res, next) {
  if (req.session.user) {
    return res.redirect('/')
  }

  res.render('login/index', {})
})

router.post(
  '/',
  body('email')
    .isEmail()
    .normalizeEmail()
    .withMessage('Invalid Email'),
  body('password')
    .not()
    .isEmpty()
    .withMessage('Missing Password'),
  function(req, res, next) {
    const validationErrors = validationResult(req).formatWith(
      ({ param, msg: message }) => `${param}: ${message}`
    )

    if (!validationErrors.isEmpty()) {
      return res.render('login/index', {
        values: req.body,
        errors: validationErrors.array({
          onlyFirstError: true
        })
      })
    }

    const { email, password } = matchedData(req)

    User.findOne({
      where: {
        email
      }
    }).then(user => {
      if (!user) {
        return res.render('login/index', {
          values: req.body,
          errors: ['Incorrect Login Credential!']
        })
      }

      const verified = verifyPassword(user.get('password'), password)

      if (!verified) {
        return res.render('login/index', {
          values: req.body,
          errors: ['Incorrect Login Credential!']
        })
      }

      req.session.user = {
        id: user.get('id')
      }

      req.session.save(err => {
        if (err) next(err)

        res.redirect('/')
      })
    })
  }
)

module.exports = router
