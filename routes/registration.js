const router = require('express').Router()

const { body } = require('express-validator/check')
const { validationResult } = require('express-validator/check')
const { matchedData } = require('express-validator/filter')

const { hashPassword } = require('../libs/password.js')

const { User } = require('../database/index.js')

router.get('/', function(req, res, next) {
  if (req.session.user) {
    return res.redirect('/')
  }

  res.render('register/index', {})
})

router.post(
  '/',
  body('firstName')
    .isString()
    .not()
    .isEmpty(),
  body('lastName')
    .isString()
    .not()
    .isEmpty(),
  body('email')
    .isEmail()
    .normalizeEmail()
    .custom(email =>
      User.findOne({ attributes: ['id'], where: { email } }).then(user => {
        if (user) throw new Error('already registered')
      })
    ),
  body('password')
    .isLength({ min: 8 })
    .withMessage('password must be at least 8 characters long'),
  body('passwordConfirmation').custom((passwordConfirmation, { req }) => {
    if (passwordConfirmation === req.body.password) return true
    throw new Error('password confirmation does not match password')
  }),
  function(req, res, next) {
    const validationErrors = validationResult(req).formatWith(
      ({ param, msg: message }) => `${param}: ${message}`
    )

    if (!validationErrors.isEmpty()) {
      return res.render('register/index', {
        values: req.body,
        errors: validationErrors.array({
          onlyFirstError: true
        })
      })
    }

    const { firstName, lastName, email, password } = matchedData(req)

    const queryData = {
      firstName,
      lastName,
      email,
      password: hashPassword(password)
    }

    User.create(queryData).then(user => {
      const data = user.toJSON()

      res.render('register/success', {
        title: 'Registration Successful!',
        firstName: data.firstName,
        lastName: data.lastName
      })
    })
  }
)

module.exports = router
